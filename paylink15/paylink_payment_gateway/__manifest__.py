
{
    'name': 'Paylink Payment Gateway',
    'category': 'Accounting/Payment Acquirers',
    'sequence': 50,
    'version': '15.0.1.0.0',
    'description': """paylink Payment Gateway""",
    'Summary': """Payment Provider : paylink """,
    'author': "Mohamed Ismail",
    'company': 'Ctit',
    'maintainer': 'Ctit',
    'website': "https://www.ctit.com",
    'depends': ['payment','account','website','website_sale'],
    'data': [
        'views/payment_template.xml',
        'views/payment_paylink_templates.xml',
        'views/paylink_payment_template.xml',
        'data/payment_provider_data.xml',

    ],
    # 'post_init_hook': 'post_init_hook',
    'uninstall_hook': 'uninstall_hook',
    'images': ['static/description/paylink.png'],
    'license': 'LGPL-3',
    'installable': True,
    'application': True,
    'auto_install': False,
}
