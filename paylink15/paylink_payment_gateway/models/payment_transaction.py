import logging
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
import requests
import json

_logger = logging.getLogger(__name__)


class PaymentTransaction(models.Model):
    _inherit = 'payment.transaction'


    paylink_transaction_no = fields.Char("Paylink Transaction No")


    def _get_specific_rendering_values(self, processing_values):
        res = super()._get_specific_rendering_values(processing_values)
        if self.provider != 'paylink':
            return res
        return self.execute_payment()

    def execute_payment(self):
        """Fetching data and Executing Payment"""
        self = self.sudo()
        paylink_provider = self.env['payment.acquirer'].search([('provider', '=', 'paylink')],limit=1)
        if not paylink_provider.id:
            raise ValidationError(_('Paylink Provider not found Or has been deleted'))
        create_invoice_url = paylink_provider._paylink_get_api_url()
        id_token = paylink_provider.get_paylink_token()
        odoo_base_url = self.env['ir.config_parameter'].get_param(
            'web.base.url')
        payment_transaction = self.env['payment.transaction'].search([('id', '=', self.id)])
        sale_order = payment_transaction.sale_order_ids

        order_line = sale_order.mapped('order_line')
        print('tok',id_token)
        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "Authorization": id_token,
        }

        products = [
            {
                "description": rec.product_id.name,
                "imageSrc": "https://merchantwebsite.com/img/img1.jpg",
                "isDigital": True,
                "price": rec.price_total,
                "productCost": 0,
                "qty": rec.product_uom_qty,
                # "specificVat": 0,
                "title": rec.product_id.name,
            }
            for rec in order_line
        ]

        payment_details = {
            "amount": self.amount,
            "callBackUrl": f"{odoo_base_url}/payment/paylink/_return_url",
            "cancelUrl": "https://www.example.com",
            "clientEmail": self.partner_email,
            "clientMobile":  self.partner_phone,
            "clientName": self.partner_name,
            "currency": sale_order.currency_id.name,
            # "note": "This invoice is for VIP client.",
            "orderNumber": sale_order.name,
            "products": products,
        }

        response = requests.post(create_invoice_url, json=payment_details, headers=headers)
        response_data = response.json()
        print(response_data)
        if not response_data.get('success'):
            raise ValidationError(f'''Title:{response_data.get('title','Some thing went Wrong')}
                                        \nDetails:{response_data.get('detail','Some thing went Wrong')}''')
        if response_data.get('success'):
            self.paylink_transaction_no = response_data.get('transactionNo',False)
        if response_data.get('url',False):
            payment_url = response_data.get('url')
            payment_details['PaymentURL'] = payment_url
        if response_data.get('qrUrl',False):
            qr_url = response_data.get('qrUrl')
            payment_details['qrURL'] = qr_url
        return {
            'api_url': f"{odoo_base_url}/payment/paylink/response",
            'data': payment_details,
        }


    @api.model
    def _get_tx_from_feedback_data(self, provider, feedback_data):
        tx = super()._get_tx_from_feedback_data(provider, feedback_data)
        if provider != 'paylink':
            return tx

        transaction_no = feedback_data.get('transaction_no')
        tx = self.search([('paylink_transaction_no', '=', transaction_no), ('provider', '=', 'paylink')])
        print('tx123456',tx)
        if not tx:
            raise ValidationError(
                "paylink: " + _("No transaction found matching reference %s.", transaction_no)
            )
        return tx

    def _process_feedback_data(self, feedback_data):
        super()._process_feedback_data(feedback_data)
        if self.provider != 'paylink':
            return

        trans_state = feedback_data.get("state", False)
        if trans_state:
            self.write({
                'state_message': _("paylink Payment Gateway Response :-") + feedback_data["state"]
            })
            if trans_state == 'done':
                self._set_done()
            elif trans_state == "pending":
                self._set_pending()
            elif trans_state == "cancel":
                self._set_canceled()

