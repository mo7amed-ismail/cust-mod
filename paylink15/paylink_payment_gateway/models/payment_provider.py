
from odoo import fields, models, api, _
import requests


class PaymentProvider(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(
        selection_add=[('paylink', "Paylink")],
        ondelete={'paylink': 'set default'}
    )
    paylink_api_id = fields.Char('Paylink Api Id')
    paylink_api_id_test = fields.Char('Test Paylink Api Id',default='APP_ID_1123453311')
    paylink_secret_key = fields.Char('Paylink Secret Key')
    paylink_secret_key_test = fields.Char('Test Paylink Secret Key',default='0662abb5-13c7-38ab-cd12-236e58f43766')
    paylink_id_token = fields.Char('Paylink Token')
    persist_token = fields.Boolean(
        string='Persist Token',
        default=False)
    paylink_last_auth = fields.Datetime(
        string='Last Authorization',
        default=False)


    def _get_default_payment_method_id(self):
        self.ensure_one()
        if self.provider != 'paylink':
            return super()._get_default_payment_method_id()
        return self.env.ref('paylink_payment_gateway.payment_method_paylink').id


    @api.onchange('state')
    def onchange_state_for_last_paylink_auth(self):
        for rec in self:
            rec.paylink_last_auth = False
            rec.paylink_id_token = False


    @api.onchange('persist_token')
    def onchange_persist_token_for_last_paylink_auth(self):
        for rec in self:
            rec.paylink_last_auth = False
            rec.paylink_id_token = False


    @api.onchange('paylink_api_id','paylink_secret_key')
    def onchange_live_api_key_for_last_paylink_auth(self):
        for rec in self:
            if rec.state == 'enabled':
                rec.paylink_last_auth = False
                rec.paylink_id_token = False


    @api.onchange('paylink_api_id_test','paylink_secret_key_test')
    def onchange_test_api_key_for_last_paylink_auth(self):
        for rec in self:
            if rec.state == 'test':
                rec.paylink_last_auth = False
                rec.paylink_id_token = False


    def get_paylink_authorization_payload(self):
        for rec in self:
            state = rec.state
            api_id_state_dict = {'disabled':False,'enabled':rec['paylink_api_id'],'test':rec['paylink_api_id_test']}
            secret_key_state_dict = {'disabled':False,'enabled':rec['paylink_secret_key'],'test':rec['paylink_secret_key_test']}
            payload = {
                "apiId": api_id_state_dict.get(state,False),
                "persistToken": rec.persist_token,
                "secretKey": secret_key_state_dict.get(state,False)
            }
            return payload


    def get_paylink_auth_time_difference(self):
        for rec in self:
            paylink_last_auth = rec.paylink_last_auth
            now = fields.Datetime.now()
            persist_token = rec.persist_token
            diff = False
            if paylink_last_auth:
                difference_time = now - paylink_last_auth
                total_seconds = difference_time.days * 24 * 60 * 60 + difference_time.seconds
                if persist_token:
                    total_hours = total_seconds / (60 * 60)
                    diff = total_hours
                else:
                    total_minutes = total_seconds / (60)
                    diff = total_minutes
            print('lllllllllllllll',diff)
            return diff


    def get_paylink_token(self):
        for rec in self:
            paylink_auth_time_difference = rec.get_paylink_auth_time_difference()
            if not rec.paylink_id_token or not paylink_auth_time_difference or paylink_auth_time_difference >= 30:
                rec.paylink_generate_authorization_token()
            return rec.paylink_id_token


    def paylink_generate_authorization_token(self):
        headers = {
            "accept": "*/*",
            "content-type": "application/json"
        }

        for rec in self:
            url = rec._paylink_get_api_url(end='auth')
            payload = rec.get_paylink_authorization_payload()
            response = requests.post(url, json=payload, headers=headers)
            res = response.json()
            print(res)
            id_token = res.get("id_token", False)
            if id_token:
                rec.paylink_id_token = id_token
                rec.paylink_last_auth = fields.Datetime.now()
            return res



    def _paylink_get_api_url(self,end='addInvoice'):
        self.ensure_one()
        state = self.state
        url_state_dict = {'enabled':' https://restapi.paylink.sa','test':' https://restpilot.paylink.sa'}
        return f"{url_state_dict.get(state,url_state_dict['test'])}/api/{end}"
