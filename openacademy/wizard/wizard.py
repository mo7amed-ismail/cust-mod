from odoo import models, fields, api
#TransientModel

class wizard(models.TransientModel):
    _name = 'openacademy.wizard'
    _description = 'Wizard : Quick registration of attendees to session'

    def _default_sessions(self):
         return self._context.get('active_ids')

  #  a = fields.Char(compute='tost')
    session_ids = fields.Many2many('openacademy.session',
                                  string='Sessions', required=True,default=_default_sessions)
    attendee_ids=fields.Many2many('res.partner' , string='Attendees')

    def subscribe(self):
        for s in self.session_ids:
            s.attendee_ids |= self.attendee_ids
            print(self._context)
        return {}

   # def tost(self):
    #        self.a=self.env['openacademy.course'].browse(12)
#return self.env['openacademy.session'].browse(self._context.get('active_ids'))