# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions
from odoo.exceptions import ValidationError
from datetime import timedelta

# class openacademy(models.Model):
#     _name = 'openacademy.openacademy'
#     _description = 'openacademy.openacademy'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100

class course(models.Model):
    _name = 'openacademy.course'
    _description = 'openacademy courses'

    def get_sel(self):
        return[(1,'jkfkj'),(2,'huweuh')]

    name = fields.Char(string='Title', required=True)
    description=fields.Text()
    responsible_id=fields.Many2one('res.users' , ondelete='set null' ,
        string='Responsible' , index=True)
    session_ids=fields.One2many('openacademy.session' , 'course_id' ,
                                    string='Sessions' )
    statee=fields.Selection([('asd','fgh'),
                            ('zxc','vbn'),
                            ('qwe','tyu')], default='qwe')
    asd=fields.Selection(get_sel,default=lambda self: self._get_daf)

    _sql_constraints = [
                        ('name_description_check',
                        'CHECK(name != description)',
                         "The title of the course shoudn't be the description"),

                          ('name_unique',
                            'UNIQUE(name)',
                         "The course title must be unique")]

    @property
    def _get_daf(self):
        return 1

    # @property
    # def get_sel(self):
    #     return[(1,'jkfkj'),(2,'huweuh')]



class session(models.Model):
    _name = 'openacademy.session'
    _description = 'openacademy sessions'


    name=fields.Char(required=True)
    start_date=fields.Date(default=fields.Date.today)
    duration=fields.Float(digits=(6, 2), help="Duration in days")
    end_date=fields.Date(string='End Date', store=True,
                         copmute='_get_end_date', inverse='_set_end_date')
    seats=fields.Integer(string='Number of seats')
    Active=fields.Boolean(default=True)
    instructor_id=fields.Many2one('res.partner' , string='Instructor' )
    course_id=fields.Many2one('openacademy.course' ,
                              ondelete='cascade' , string='Course' , required=True)
    attendee_ids=fields.Many2many('res.partner' , string='Attendees')
    taken_seats=fields.Float(string='Taken Seats' , compute='_taken_seats' )
    attendee_count=fields.Integer(compute='_get_attendees_count', store=True, string='Attendees count')
   # a=fields.Char('openacademy.course',compute='tost')

  #  def tost(self):
   #     con={'s':1,'b':2,'c':5}
    #    for r in self:
     #       r.a=
    
    @api.depends('seats','attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0
            else:
                r.taken_seats = (len(r.attendee_ids) / r.seats) * 100
    @api.depends('attendee_ids')
    def _get_attendees_count(self):
        for r in self:
            r.attendee_count=len(r.attendee_ids)


    @api.onchange('seats', 'attendee_ids')
    def _verify_valid_seats(self):
        for r in self:
            if r.seats<0:
                return {'warning':{'title':'Incorrect number of seats',
                                        'message':'The number of available seats must be positive'}
                             }
            if r.seats<len(r.attendee_ids):
                return {'warning': {'title': 'Too many number of attendees',
                                         'message': 'Increase seats or remove excess attendees'}
                             }

    @api.onchange('start_date', 'duration')
    def _get_end_date(self):
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue

            durationn = timedelta(days=r.duration, seconds=-1)
            r.end_date = r.start_date + durationn


    def _set_end_date(self):
        for r in self:
            if not (r.start_date and r.end_date):
                continue

            r.duration=(r.end_date - r.start_date).days + 1


    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instruc_not_in_attendees(self):
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                raise ValidationError("A session instructor can't be an attendee")







