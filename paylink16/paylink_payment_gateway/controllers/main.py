import logging
import pprint
import json
import requests
from odoo import http
from odoo.http import request
import ast

_logger = logging.getLogger(__name__)


class PaymentpaylinkController(http.Controller):
    _return_url = '/payment/paylink/_return_url'

    @http.route('/payment/paylink/response', type='http', auth='public',
                website=True, methods=['POST'], csrf=False, save_session=False)
    def paylink_payment_response(self, **data):
        payment_data = ast.literal_eval(data["data"])
        vals = {
            'customer': payment_data["clientName"],
            'currency': payment_data["currency"],
            # 'country_code': payment_data["MobileCountryCode"],
            'mobile': payment_data["clientMobile"],
            'email': payment_data["clientEmail"],
            'invoice_amount': payment_data["amount"],
            # 'address': payment_data["CustomerAddress"]["Address"],
            'payment_url': payment_data["PaymentURL"],
            'qr_url': payment_data["qrURL"],
        }
        return request.render(
            "paylink_payment_gateway.paylink_payment_gateway_form", vals)

    @http.route(_return_url, type='http', auth='public',
                methods=['GET'])
    def paylink_checkout(self, **post):
        print("get_paylink_payment_status  ==========================", post)
        transaction_no = post.get('transactionNo', False)
        order_no = post.get('orderNumber', False)
        payment_errors = post.get('paymentErrors',False)
        if transaction_no and not payment_errors:
            data = {
                'state': 'done',
                'transaction_no': transaction_no,
            }
            payment_transaction = request.env['payment.transaction'].sudo()._handle_notification_data("paylink", data)
            return request.redirect("/payment/status")
        else:
            error_message = post.get(payment_errors,'Some Thing went wrong, Payment Not Done')
            value = {
                'error': error_message,
                'redirect': '/payment/status',
            }
            data = {
                'state': 'cancel',
                'transaction_no': transaction_no,
            }
            payment_transaction = request.env['payment.transaction'].sudo()._handle_notification_data("paylink", data)
            return request.render('paylink_payment_gateway.paylink_payment_gateway_failed_form', value)





    @http.route('/payment/paylink/failed', type='http', auth='user',
                website=True, )
    def payment_failed(self, redirect=None, **data):
        # self.paylink_checkout(data = data)
        return request.render(
            "paylink_payment_gateway.paylink_payment_gateway_failed_form")
