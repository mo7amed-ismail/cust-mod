# -*- coding: utf-8 -*-
{
    'name': "Hospital Management",

    'summary': """
        Module For Managing The Hospitals""",

    'description': """
        Module For Managing The Hospitals
    """,
    'sequence': '1',
    'author': "Eng Mahmoud Sayed",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'hospitals',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mail', 'product'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/sequence.xml',
        'data/patient_tag_data.xml',
        'data/patient.tag.csv',
        'data/mail_template_data.xml',
        'wizard/cancel_appointment_view.xml',
        'views/patient_views.xml',
        'views/templates.xml',
        'views/Appointment.xml',
        'views/patient_tag_view.xml',
        'views/res_config_settings_views.xml',
        'views/operation.xml',
        'views/odoo_playground_view.xml',
        'report/patient_details_template.xml',
        'report/report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
