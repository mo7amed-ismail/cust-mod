# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from odoo.exceptions import ValidationError
from dateutil import relativedelta
from datetime import date

class AppointmentCancellation(models.TransientModel):
    _name = 'appointment.cancellation'
    _description = 'Appointment Cancellation'

    @api.model
    def default_get(self, fields):
        res = super(AppointmentCancellation, self).default_get(fields)
        res['date_cancel'] = date.today()
        if self.env.context.get('active_id'):
            res["appointment_id"] = self.env.context.get('active_id')
        return res

    appointment_id = fields.Many2one('hospital.appointment', string='Appointment')
    reason = fields.Text('the Reason')
    date_cancel = fields.Date('cancellation Date')

    @api.depends('appointment_id')
    def appointment_cancel(self):
        cancel_days = self.env['ir.config_parameter'].get_param('om_hospital.cancel_days')
        allowed_date = self.appointment_id.booking_date - relativedelta.relativedelta(days=int(cancel_days))
        if allowed_date < date.today():
            raise ValidationError(_("Sorry. Cancellation is not allowed for this booking !"))
        self.appointment_id.state = 'cancel'
        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
        }




