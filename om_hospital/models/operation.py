from odoo import api, fields, models 


class HospitalOperation(models.Model):
    _name = 'hospital.operation'
    _description = 'Hospital Operation'
    _rec_name = 'name'
    _order = 'sequence,id'

    name = fields.Char('Name')
    doctor_id = fields.Many2one('res.users', string='Doctor')
    reference_record = fields.Reference(selection=[('hospital.patient', 'Patient'),
                                                   ('hospital.appointment', 'Appointment')], string='Record')
    sequence = fields.Integer('Sequence', default='10')
