# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import date
from dateutil import relativedelta
from odoo.exceptions import ValidationError


class HospitalPatient(models.Model):
    _name = 'hospital.patient'
    _description = 'Patient Information'
    _rec_name = 'name'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = 'id desc'

    name = fields.Char(string='Patient Name', tracking=True, required=True)
    notes = fields.Text(string='Registration Notes', tracking=True)
    ref = fields.Char(string='Patient ID', required=True, copy=False,
                      readonly=True, index=True, default=lambda self: _('New'))
    image = fields.Image('Image')
    active = fields.Boolean(string='Active', tracking=True, default=True)
    dob = fields.Date('Date Of Birth')
    age = fields.Integer('Age', tracking=True, store=True, compute='compute_age', inverse='_inverse_compute_age',
                         search='_search_age')
    gender = fields.Selection([('male', 'Male'), ('female', 'Female')], 'Gender')
    age_group = fields.Selection([('major', 'Major'), ('minor', 'Minor')], 'Age Group',
                                 compute='get_age_group', store=True)
    tags_ids = fields.Many2many('patient.tag', string='Tags')
    appointment_id = fields.Many2one('hospital.appointment', string='Appointments')
    appointment_ids = fields.One2many('hospital.appointment', 'patient_id', string='Appointments')
    appointment_count = fields.Integer(string='Appointment count', compute='_compute_appointment_count')
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')], 'Marital Status')
    phone = fields.Char('Phone')
    email = fields.Char('Email')
    parent = fields.Char('Parent')
    partner = fields.Char('Partner Name')
    is_birthday = fields.Boolean('Birthday ?', compute='_compute_is_birthday')

    @api.depends('dob')
    def _compute_is_birthday(self):
        for rec in self:
            rec.is_birthday = False
            if rec.dob:
                today = date.today()
                if today.day == rec.dob.day and today.month == rec.dob.month:
                    rec.is_birthday = True

    @api.depends('appointment_ids')
    def _compute_appointment_count(self):
        # appointment_group = self.env['hospital.appointment'].read_group(domain=[], fields=['patient_id'],
        #                                                                 groupby=['patient_id'])
        # print('appppppppppp', appointment_group)
        # for appointment in appointment_group:
        #     print('appointment', appointment)
        #     patient_id = appointment.get('patient_id')[0]
        #     print('patient', patient_id)
        #     patient_rec = self.browse(patient_id)
        #     print('patient_rec', patient_rec)
        #     patient_rec.appointment_count = appointment['patient_id_count']
        #     print('patient_rec.appointment_count', patient_rec.appointment_count)
        #     self -= patient_rec
        #     print('self', self)
        # self.appointment_count = 0
        for rec in self:
            rec.appointment_count = self.env['hospital.appointment'].search_count([('patient_id', '=', rec.id)])

    def action_view_appointment(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'hospital.appointment',
            'name': 'Appointments',
            'view_mode': 'list,form',
            'domain': [('patient_id', '=', self.id)],
            'target': 'current',
            'context': {'default_patient_id': self.id},
        }

    @api.depends('age')
    def get_age_group(self):
        for rec in self:
            if rec.age:
                if rec.age < 18:
                    rec.age_group = 'minor'
                else:
                    rec.age_group = 'major'

    @api.model
    def create(self, vals):
        if vals.get('ref', _('New')) == _('New'):
            vals['ref'] = self.env['ir.sequence'].next_by_code('hospital.patient.sequence') or _('New')
            print('Values', vals)
        result = super(HospitalPatient, self).create(vals)
        print('res', result)
        return result

    def write(self, vals):
        if not self.ref and not vals.get('ref'):
            vals['ref'] = self.env['ir.sequence'].next_by_code('hospital.patient.sequence')
        res = super(HospitalPatient, self).write(vals)
        return res

    def create_appointment(self):
        for rec in self:
            app_vals = {
                'patient_id': self.id,
                'doctor_id': self.env.user.id,
                'priority': '2',
                'user': self.create_uid.name,
                'object': self._name,
                'user_id': self.create_uid.id,
                'object_id': self.env['ir.model'].search([('model', '=', self._name)]).id,
                'record': self.id,
            }
            appointment_id = rec.env['hospital.appointment'].create(app_vals)
            appointment_id.action_in_consultation()

    @api.depends('dob')
    def compute_age(self):
        for r in self:
            if r.dob:
                r.age = date.today().year - r.dob.year
            else:
                r.age = 0

    @api.depends('age')
    def _inverse_compute_age(self):
        today = date.today()
        for rec in self:
            rec.dob = today - relativedelta.relativedelta(years=rec.age)

    def _search_age(self, operator, value):
        dob = date.today() - relativedelta.relativedelta(years=value)
        start_of_year = dob.replace(day=1, month=1)
        end_of_year = dob.replace(day=31, month=12)
        return [('dob', '>=', start_of_year), ('dob', '<=', end_of_year)]

    def name_get(self):
        # patient_list = []
        # for rec in self:
        #     name = rec.ref + ' ' + rec.name
        #     patient_list.append((rec.id,name))
        # return patient_list
        return [(rec.id, "[%s] %s" % (rec.ref, rec.name)) for rec in self]

    # @api.model
    # def create(self, vals):
    #     print('yessssssss', vals)
    #     vals['name'] = 'Odoo Test'
    #     vals['notes'] = 'Odoo Test'
    #     vals['age'] = '25'
    #     vals['ref'] = 'Hp009'
    #     res = super(HospitalPatient, self).create(vals)
    #     print(res)
    #     print('yessssssss', vals)
    #     return res



