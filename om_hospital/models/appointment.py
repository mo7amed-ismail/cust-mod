from odoo import api, fields, models, _
from odoo.exceptions import AccessError, UserError, ValidationError
from odoo.tools import float_is_zero, html_keep_url, is_html_empty
from itertools import groupby


class HospitalAppointment(models.Model):
    _name = 'hospital.appointment'
    _description = 'Hospital Appointment'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'seq'
    _order = 'seq desc'

    patient_id = fields.Many2one('hospital.patient', string='Patient')
    doctor_id = fields.Many2one('res.users', string='Doctor')
    operation_id = fields.Many2one('hospital.operation', string='Operation')
    ref = fields.Char('Reference')
    pharmacy_line_ids = fields.One2many('appointment.pharmacy.lines', 'appointment_id',
                                        string='Pharmacy Lines')
    gender = fields.Selection(related='patient_id.gender')
    hide_price_column = fields.Boolean(string='Hide price column')
    appointment_time = fields.Datetime('Appointment Time', default=fields.Datetime.now)
    booking_date = fields.Date('Booking Date', default=fields.Date.context_today)
    priority = fields.Selection([
        ('0', 'Low'),
        ('1', 'Medium'),
        ('2', 'High'),
        ('3', 'Very High')],
        string='Priority')
    state = fields.Selection(
        string='Status',
        selection=[('draft', 'Draft'),
                   ('in_consultation', 'In Consultation'),
                   ('done', 'Done'),
                   ('cancel', 'Cancelled')],
        required=True, default='draft')
    seq = fields.Char('Sequence')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id')
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, compute='_amount_all')
    attachment = fields.Binary('Attachment')
    user = fields.Char('User Name')
    object = fields.Char('Object Name')
    user_id = fields.Many2one('res.users', 'User')
    object_id = fields.Many2one('ir.model', 'Object')
    record = fields.Char('Record ID')

    @api.depends('pharmacy_line_ids.subtotal_price')
    def _amount_all(self):
        for order in self:
            amount_total = order.pharmacy_line_ids.subtotal_price = 0.0
            for line in order.pharmacy_line_ids:
                line._compute_amount()
                amount_total += line.subtotal_price
            currency = order.currency_id or self.env.company.currency_id
            order.update({
                'amount_total': currency.round(amount_total)})
            print('amount_total', amount_total)
            return amount_total

    @api.model
    def create(self, vals):
        if vals.get('seq', _('New')) == _('New'):
            vals['seq'] = self.env['ir.sequence'].next_by_code('hospital.appointment.sequence') or _('New')
            print('Values', vals)
        result = super(HospitalAppointment, self).create(vals)
        print('res', result)
        return result

    def write(self, vals):
        if not self.seq and not vals.get('seq'):
            vals['seq'] = self.env['ir.sequence'].next_by_code('hospital.appointment.sequence')
        return super(HospitalAppointment, self).write(vals)

    @api.onchange('patient_id')
    def onchange_patient_id(self):
        for rec in self:
            rec.ref = rec.patient_id.ref

    def action_in_consultation(self):
        for rec in self:
            if rec.state == 'draft':
                rec.state = 'in_consultation'

    def action_done(self):
        for rec in self:
            if rec.state == 'in_consultation':
                rec.state = 'done'

    def action_cancel(self):
        action = self.env.ref('om_hospital.appointment_cancellation_action').read()[0]
        return action

    def action_draft(self):
        for rec in self:
            rec.state = 'draft'

    def action_share_whatsapp(self):
        if not self.patient_id.phone:
            raise ValidationError(_("Missing phone number in patient record !"))
        if not self.attachment:
            raise ValidationError(_("Missing Attachment!"))
        message = '*%s*' % (self.attachment)
        # message = 'Hi *%s*, your appointment number is: *%s* at date: *%s*, thank you' % (self.patient_id.name, self.seq,self.appointment_time)
        whatsapp_api_url = 'https://api.whatsapp.com/send?phone=%s&text=%s' % (self.patient_id.phone, message)
        self.message_post(body=message, subject='Whatsapp Message')
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': whatsapp_api_url
        }

    def action_notification(self):
        action = self.env.ref(
            'om_hospital.hospital_patient_male_action' and 'om_hospital.hospital_patient_female_action')
        return {
            'type': 'ir.actions.client',
            'tag': 'display_notification',
            'params': {
                'title': _('Click To Open The Patient Record'),
                'message': '%s',
                'links': [{
                    'label': self.patient_id.name,
                    'url': f'#action={action.id}&id={self.patient_id.id}&model=hospital.patient'
                }],
                'sticky': False,
                'next': {
                    'type': 'ir.actions.act_window',
                    'res_model': 'hospital.patient',
                    'res_id': self.patient_id.id,
                    'views': [(False, 'form')]
                }
            }
        }

    def action_send_email(self):
        template_id = self.env.ref('om_hospital.appointment_mail_template').id
        for rec in self:
            if rec.patient_id.email:
                self.env['mail.template'].browse(template_id).send_mail(self.id, force_send=True)
        print('success to send email')

    def _prepare_invoice(self):
        self.ensure_one()
        journal = self.env['account.move'].with_context(default_move_type='out_invoice')._get_default_journal()
        if not journal:
            raise UserError(
                _('Please define an accounting sales journal for the company %s (%s).', self.company_id.name,
                  self.company_id.id))

        invoice_vals = {
            'ref': self.patient_id.ref or '',
            'move_type': 'out_invoice',
            'narration': self.patent_id.notes,
            'currency_id': self.currency_id.id,
            'source_id': self.object_id.id,
            'user_id': self.user_id.id,
            'invoice_user_id': self.user_id.id,
            'partner_id': self.patient_id.id,
            'partner_bank_id': self.company_id.partner_id.bank_ids.filtered(
                lambda bank: bank.company_id.id in (self.company_id.id, False))[:1].id,
            'journal_id': journal.id,  # company comes from the journal
            'invoice_origin': self.name,
            'payment_reference': self.seq,
            'invoice_line_ids': [],
            'company_id': self.company_id.id,
        }
        return invoice_vals

    def _get_invoiceable_lines(self, final=False):
        """Return the invoiceable lines for order `self`."""
        down_payment_line_ids = []
        invoiceable_line_ids = []
        pending_section = None
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')

        for line in self.order_line:
            if line.display_type == 'line_section':
                # Only invoice the section if one of its lines is invoiceable
                pending_section = line
                continue
            if line.display_type != 'line_note' and float_is_zero(line.qty_to_invoice, precision_digits=precision):
                continue
            if line.qty_to_invoice > 0 or (line.qty_to_invoice < 0 and final) or line.display_type == 'line_note':
                if line.is_downpayment:
                    # Keep down payment lines separately, to put them together
                    # at the end of the invoice, in a specific dedicated section.
                    down_payment_line_ids.append(line.id)
                    continue
                if pending_section:
                    invoiceable_line_ids.append(pending_section.id)
                    pending_section = None
                invoiceable_line_ids.append(line.id)

        return self.env['sale.order.line'].browse(invoiceable_line_ids + down_payment_line_ids)

    def _create_invoices(self, grouped=False, final=False, date=None):
        if not self.env['account.move'].check_access_rights('create', False):
            try:
                self.check_access_rights('write')
                self.check_access_rule('write')
            except AccessError:
                return self.env['account.move']

        # 1) Create invoices.
        invoice_vals_list = []
        invoice_item_sequence = 0  # Incremental sequencing to keep the lines order on the invoice.
        for order in self:
            order = order.with_company(order.company_id)
            current_section_vals = None
            down_payments = order.env['sale.order.line']

            invoice_vals = order._prepare_invoice()
            invoiceable_lines = order._get_invoiceable_lines(final)

            if not any(not line.display_type for line in invoiceable_lines):
                continue

            invoice_line_vals = []
            down_payment_section_added = False
            for line in invoiceable_lines:
                if not down_payment_section_added and line.is_downpayment:
                    # Create a dedicated section for the down payments
                    # (put at the end of the invoiceable_lines)
                    invoice_line_vals.append(
                        (0, 0, order._prepare_down_payment_section_line(
                            sequence=invoice_item_sequence,
                        )),
                    )
                    down_payment_section_added = True
                    invoice_item_sequence += 1
                invoice_line_vals.append(
                    (0, 0, line._prepare_invoice_line(
                        sequence=invoice_item_sequence,
                    )),
                )
                invoice_item_sequence += 1

            invoice_vals['invoice_line_ids'] += invoice_line_vals
            invoice_vals_list.append(invoice_vals)

        if not invoice_vals_list:
            raise self._nothing_to_invoice_error()

        # 2) Manage 'grouped' parameter: group by (partner_id, currency_id).
        if not grouped:
            new_invoice_vals_list = []
            invoice_grouping_keys = self._get_invoice_grouping_keys()
            invoice_vals_list = sorted(
                invoice_vals_list,
                key=lambda x: [
                    x.get(grouping_key) for grouping_key in invoice_grouping_keys
                ]
            )
            for grouping_keys, invoices in groupby(invoice_vals_list,
                                                   key=lambda x: [x.get(grouping_key) for grouping_key in
                                                                  invoice_grouping_keys]):
                origins = set()
                payment_refs = set()
                refs = set()
                ref_invoice_vals = None
                for invoice_vals in invoices:
                    if not ref_invoice_vals:
                        ref_invoice_vals = invoice_vals
                    else:
                        ref_invoice_vals['invoice_line_ids'] += invoice_vals['invoice_line_ids']
                    origins.add(invoice_vals['invoice_origin'])
                    payment_refs.add(invoice_vals['payment_reference'])
                    refs.add(invoice_vals['ref'])
                ref_invoice_vals.update({
                    'ref': ', '.join(refs)[:2000],
                    'invoice_origin': ', '.join(origins),
                    'payment_reference': len(payment_refs) == 1 and payment_refs.pop() or False,
                })
                new_invoice_vals_list.append(ref_invoice_vals)
            invoice_vals_list = new_invoice_vals_list

        # 3) Create invoices.

        # As part of the invoice creation, we make sure the sequence of multiple SO do not interfere
        # in a single invoice. Example:
        # SO 1:
        # - Section A (sequence: 10)
        # - Product A (sequence: 11)
        # SO 2:
        # - Section B (sequence: 10)
        # - Product B (sequence: 11)
        #
        # If SO 1 & 2 are grouped in the same invoice, the result will be:
        # - Section A (sequence: 10)
        # - Section B (sequence: 10)
        # - Product A (sequence: 11)
        # - Product B (sequence: 11)
        #
        # Resequencing should be safe, however we resequence only if there are less invoices than
        # orders, meaning a grouping might have been done. This could also mean that only a part
        # of the selected SO are invoiceable, but resequencing in this case shouldn't be an issue.
        if len(invoice_vals_list) < len(self):
            SaleOrderLine = self.env['sale.order.line']
            for invoice in invoice_vals_list:
                sequence = 1
                for line in invoice['invoice_line_ids']:
                    line[2]['sequence'] = SaleOrderLine._get_invoice_line_sequence(new=sequence,
                                                                                   old=line[2]['sequence'])
                    sequence += 1

        # Manage the creation of invoices in sudo because a salesperson must be able to generate an invoice from a
        # sale order without "billing" access rights. However, he should not be able to create an invoice from scratch.
        moves = self.env['account.move'].sudo().with_context(default_move_type='out_invoice').create(invoice_vals_list)

        # 4) Some moves might actually be refunds: convert them if the total amount is negative
        # We do this after the moves have been created since we need taxes, etc. to know if the total
        # is actually negative or not
        if final:
            moves.sudo().filtered(lambda m: m.amount_total < 0).action_switch_invoice_into_refund_credit_note()
        for move in moves:
            move.message_post_with_view('mail.message_origin_link',
                                        values={'self': move, 'origin': move.line_ids.mapped('sale_line_ids.order_id')},
                                        subtype_id=self.env.ref('mail.mt_note').id
                                        )
        return moves


class AppointmentPharmacyLines(models.Model):
    _name = 'appointment.pharmacy.lines'
    _description = 'Appointment Pharmacy Lines'

    product_id = fields.Many2one('product.product')
    price_unit = fields.Float(related='product_id.list_price', string='Price', digits='Product Price')
    qty = fields.Integer('Quantity', default="1")
    subtotal_price = fields.Monetary(compute='_compute_amount', string='Subtotal', store=True)
    currency_id = fields.Many2one('res.currency', related='appointment_id.currency_id')
    appointment_id = fields.Many2one('hospital.appointment', string='Appointment')
    price_total = fields.Monetary(compute='_compute_amount', string='Total', store=True)

    @api.depends('product_id', 'price_unit', 'qty')
    def _compute_amount(self):
        for line in self:
            line.price_total = line.subtotal_price = 0.0
            if line.product_id:
                print('gggggggggggggggggggggggggggg')
                line.subtotal_price = line.price_unit * line.qty
                line.price_total += line.subtotal_price
                print('total', line.price_total)
        line.update({'price_total': line.price_unit})
        return line.price_total
