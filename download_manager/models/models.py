# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from odoo.exceptions import ValidationError
from tkinter import filedialog
from pytube import YouTube
import os, sys



class DownloadManager(models.Model):
    _name = 'download.manager'
    _description = 'Download Manager'

    folder_name = fields.Char('Directory')
    video_name = fields.Char('Video Name', default="Video Name: ")
    url = fields.Char('The URL')
    stream = fields.Selection(
        string='Quality',
        selection=[('360p', '360p'),
                   ('720p', '720p'), ],
        required=True, default='360p')
    state = fields.Selection(
        string='State',
        selection=[('ready', 'Ready....'),
                   ('download', 'Downloading....'),
                   ('complete', 'Download Completed....'), ],
        default='ready', )
    name = fields.Char('Download Manager', default='Download Manager',compute='_get_name')


    def _get_name(self):
        for rec in self:
            rec.name = '  '

    def browse_folder(self):
        directory = filedialog.askdirectory(title="Save Video")
        self.folder_name = False
        self.folder_name = directory

    def download(self):
        self.state = 'download'
        print('Downloading......')
        link = self.url
        if not link:
            raise ValidationError(_("Put The Youtube Video Link First! :("))
        print('link:  ', link)
        folder = self.folder_name
        if not folder:
            raise ValidationError(_("Choose The Directory First! :("))
        print('folder: ', folder)
        available_streams = YouTube(link, use_oauth=True, allow_oauth_cache=True).streams.filter(progressive=True, file_extension="mp4")
        for stm in available_streams:
            print(stm)
        stream = self.stream
        if stream:
            print(stream)
            video = YouTube(link, use_oauth=True, allow_oauth_cache=True).streams.filter(progressive=True,file_extension="mp4",res=stream).get_by_resolution(stream)
            self.video_name = video.title
            print('video_name: ',self.video_name)
            video.download(folder)
            self.state = 'complete'
            print("download completed successfully :)")
            return {
                'effect': {
                    'fadeout': 'slow',
                    'message': "download completed successfully :)",
                    'type': 'rainbow_man',
                }
            }

    def clear(self):
        self.url = False
        self.folder_name = False
        self.video_name = False
        self.state = 'ready'
        # os.execv(sys.executable, ["python"] + sys.argv)
