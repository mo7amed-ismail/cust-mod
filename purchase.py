# -*- coding: utf-8 -*-

from odoo import fields,models,api,_
from datetime import datetime


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.multi
    def button_confirm(self):
        res = super(PurchaseOrder, self).button_confirm()
        for picking_id in self.picking_ids:
            for move_id in picking_id.move_lines:
                move_id.generate_auto_serial_lot()
        return res

    @api.multi
    def get_unique_lot_serial_number(self, product_id, lot_serial_type):
        
        for record in self:
            # if lot_serial_type == 'purchase_date':
            #     date = record.date_order.strftime('%Y%m%d')
            # elif lot_serial_type == 'scheduled_date':
            #     date = record.date_planned.strftime('%Y%m%d')
            # else:
            #     date = datetime.now().strftime('%Y%m%d')
            date = datetime.now().strftime('%y%m')
            counter = 1
            lot_ids = self.env['stock.production.lot'].search([('name',"ilike", date)])
            if lot_ids:
                counter = len(lot_ids) + 1
            
            lot_id_name = date + str(counter).zfill(4)
            lot_id = self.env['stock.production.lot'].create({
                "product_id": product_id.id,
                "name":lot_id_name
                })
            return lot_id


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.one
    def generate_auto_serial_lot(self):
        if self.product_uom_qty != self.quantity_done:
            lot_serial_type = self.env['ir.config_parameter'].sudo().get_param('mai_auto_lotserial_incomingship.purchase_lot_serial_method')
            qty_lot = self.product_uom_qty - self.quantity_done
            if self.product_id.tracking == 'lot':
                if self.move_line_ids:
                    for move_line_id in self.move_line_ids:
                        lot_id = self.picking_id.purchase_id.get_unique_lot_serial_number(self.product_id, lot_serial_type)
                        move_line_id.write({
                            'lot_id': lot_id.id,
                            'lot_name': lot_id.name,
                            'product_uom_qty': qty_lot,
                            'qty_done': qty_lot, 
                            'product_uom_id': self.product_id.uom_id.id,
                            'location_id': self.location_id.id, 
                            'location_dest_id': self.location_dest_id.id})
                else:
                    lot_id = self.picking_id.purchase_id.get_unique_lot_serial_number(self.product_id, lot_serial_type)
                    self.env['stock.move.line'].create({
                                                'lot_id': lot_id.id,
                                                'lot_name': lot_id.name,
                                                'product_uom_qty': qty_lot,
                                                'qty_done': qty_lot,
                                                'product_uom_id': self.product_id.uom_id.id,
                                                'location_id': self.location_id.id,
                                                'location_dest_id': self.location_dest_id.id,
                                                'move_id': self.id,
                                                'product_id': self.product_id.id})
                self.write({
                    'quantity_done': qty_lot
                })
            elif self.product_id.tracking == 'serial':
                if self.move_line_ids:
                    for move_line_id in self.move_line_ids:
                        lot_id = self.picking_id.purchase_id.get_unique_lot_serial_number(self.product_id, lot_serial_type)
                        move_line_id.write({
                            'lot_id': lot_id.id,
                            'lot_name': lot_id.name,
                            'product_uom_qty': 1,
                            'qty_done': 1,
                            'product_uom_id': self.product_id.uom_id.id,
                            'location_id': self.location_id.id, 
                            'location_dest_id': self.location_dest_id.id
                        })
                else:
                    SML_obj =self.env['stock.move.line']
                    for line in range(int(self.product_uom_qty)):
                        lot_id = self.picking_id.purchase_id.get_unique_lot_serial_number(self.product_id, lot_serial_type)
                        SML_obj.create({
                           'lot_id': lot_id.id,
                           'lot_name': lot_id.name,
                           'product_uom_qty':1.0,
                           'qty_done':1.0,
                           'product_uom_id':self.product_id.uom_id.id,
                           'location_id': self.location_id.id,
                           'location_dest_id': self.location_dest_id.id,
                           'move_id': self.id,
                           'product_id': self.product_id.id
                        })
