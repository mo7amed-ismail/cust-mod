# -*- coding: utf-8 -*-
{
    'name': "ctit_theme_v15",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web', 'base_setup', 'portal', 'resource'],
    'assets': {
        'web.assets_backend': [
            # Qweb files
            '/ctit_theme_v15/static/src/xml/menu.xml',
            '/ctit_theme_v15/static/src/xml/base.xml',

            # scss files
            "/ctit_theme_v15/static/src/scss/custom_varibles.scss",
            "/ctit_theme_v15/static/src/scss/font_icons.scss",
            "/ctit_theme_v15/static/src/scss/common_view.scss",
            "/ctit_theme_v15/static/src/scss/appdrawer.scss",
            "/ctit_theme_v15/static/src/scss/side_menu.scss",
            "/ctit_theme_v15/static/src/scss/website_menu.scss",

            # js files
            "/ctit_theme_v15/static/src/js/color_pallet.js",
            "/ctit_theme_v15/static/src/js/menu.js",
            "/ctit_theme_v15/static/src/js/user_menu.js",
            "/ctit_theme_v15/static/src/js/SwitchCompanyMenu.js",
            "/ctit_theme_v15/static/src/js/menu_service.js",
        ],
    },

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
