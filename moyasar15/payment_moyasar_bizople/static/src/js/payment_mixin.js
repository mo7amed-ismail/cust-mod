odoo.define('payment_moyasar_bizople.payment_form_mixin_moyasar', require => {
    'use strict';

    const checkoutForm = require('payment.checkout_form');
    const manageForm = require('payment.manage_form');

    const MoyasarPaymentCheckout = {
    	_processRedirectPayment: function (provider, paymentOptionId, processingValues) {
            if (provider !== 'moyasar'){
            console.log(147852)
                return this._super(...arguments);
            }
            console.log(99999987)
            $('#moyasarpayment').modal('show');
            $('#wrapwrap').css('z-index',1051)
            return
        },
    }

    checkoutForm.include(MoyasarPaymentCheckout);
    manageForm.include(MoyasarPaymentCheckout);
})