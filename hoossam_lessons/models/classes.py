# -*- coding: utf-8 -*-



from odoo import models, fields, api


class classes(models.Model):
    _name = 'hossam_classes'
    _description = 'This model contains main classes and thier constant information'

    name = fields.Char(string = 'Grade' ,required=True)
    students_no = fields.Integer(string='Number of students',compute='students_count',readonly=True)
    month_fees = fields.Float(string='Month fees')
    documents_fees = fields.Float(string='Documents fees')
    notes = fields.Text()
    students_ids = fields.One2many('student_profile' , 'year_id' , string= 'Students')
    active=fields.Boolean()


    def f(self):
        for r in self:
            print(r._fields)
            #print(s) for s in r._fields
            for s in r._fields:
                print(getattr(r,s))
          #  print('fie', str(getattr(r,(fi for fi in r._fields)))
        print(self.env.ref('hoossam_lessons.hossam_classes_form').read()[0])



    def test(self):
        for r in self:
            r.write({'name':'yas'})

    @api.depends('students_ids')
    def students_count(self):
        for year in self:
            year.students_no=len(year.students_ids)
