# -*- coding: utf-8 -*-

import os
import shutil

from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    ultramsg_instance = fields.Char('Whatsapp Instance')
    ultramsg_token = fields.Char('Whatsapp Token')

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrDefault = self.env['ir.default'].sudo()
        res.update({
            'ultramsg_instance': IrDefault.get('res.config.settings', 'ultramsg_instance'),
            'ultramsg_token': IrDefault.get('res.config.settings', 'ultramsg_token'),
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        IrDefault = self.env['ir.default'].sudo()
        IrDefault.set('res.config.settings', 'ultramsg_instance', self.ultramsg_instance)
        IrDefault.set('res.config.settings', 'ultramsg_token', self.ultramsg_token)

        return True

