# -*- coding: utf-8 -*-

import base64
import datetime
import logging
import os
import time
import traceback
import subprocess
import requests


from lxml import etree

from odoo import api, fields, models, _
from odoo.exceptions import UserError


_logger = logging.getLogger(__name__)


try:
    import phonenumbers
    from phonenumbers.phonenumberutil import region_code_for_country_code
    _sms_phonenumbers_lib_imported = True

except ImportError:
    _sms_phonenumbers_lib_imported = False
    _logger.info(
        "The `phonenumbers` Python module is not available. "
        "Phone number validation will be skipped. "
        "Try `pip3 install phonenumbers` to install it."
    )
msg_sent = False
error_msg = ''

dir_path = os.path.dirname(os.path.realpath(__file__))


class SendWAMessage(models.TransientModel):
    _name = 'whatsapp.msg'
    _description = 'Send WhatsApp Message'

    def _default_unique_user(self):
        IPC = self.env['ir.config_parameter'].sudo()
        dbuuid = IPC.get_param('database.uuid')
        return dbuuid + '_' + str(self.env.uid)

    partner_ids = fields.Many2many(
        'res.partner', 'whatsapp_msg_res_partner_rel',
        'wizard_id', 'partner_id', 'Recipients')
    message = fields.Text('Message', required=True)
    attachment_ids = fields.Many2many(
        'ir.attachment', 'whatsapp_msg_ir_attachments_rel',
        'wizard_id', 'attachment_id', 'Attachments')
    unique_user = fields.Char(default=_default_unique_user)
    sending_mode = fields.Selection([('numbers', 'Numbers'), ('group', 'Group')], string='Sending Mode', default='numbers', help='Select mode for norma chat of WhatsApp Group')
    group_name = fields.Char(string='Group Name', help='Give extact name you want to send the message to WhatsApp group')




    def send_ws_msg(self,partner):
        number = partner.mobile
        url = 'https://imapi.bevatel.com/whatsapp/api/message'
        auth = 'fdkdkdk'
        ctx = self._context

        headers = {
            "Authorization": auth,
            "Content-Type": "application/json"
        }
        payload = {
            "phone": number,
            # "channelId": 123456,
            "templateName": "sample_issue_resolution",
            "languageCode": ctx.get("lang","en_US"),
            "text": self.message,
            # "parameters": [
            #     "name"
            # ],
            # "image": "https://via.placeholder.com/150",
            # "document": "https://s2.q4cdn.com/175719177/files/doc_presentations/Placeholder-PDF.pdf",
            # "video": "https://cdn.videvo.net/videvo_files/video/free/2013-08/large_watermarked/hd0983_preview.mp4",
            # "tags": [
            #     "api",
            #     "test"
            # ],
            # "custom_fields": [
            #     {
            #         "name": "fieldName",
            #         "value": "fieldValue"
            #     }
            # ]
        }
        if self.attachment_ids.ids:
            pdf_url = self.attachment_ids[0].datas
            payload.update({"document": pdf_url, })

        response = requests.request('POST', url, data=payload, headers=headers)
        print(response.text)    # def send_ws_msg(self,partner):
        print(response.json())





    def format_amount(self, amount, currency):
        fmt = "%.{0}f".format(currency.decimal_places)
        lang = self.env['res.lang']._lang_get(self.env.context.get('lang') or 'en_US')

        formatted_amount = lang.format(fmt, currency.round(amount), grouping=True, monetary=True)\
            .replace(r' ', u'\N{NO-BREAK SPACE}').replace(r'-', u'-\N{ZERO WIDTH NO-BREAK SPACE}')

        pre = post = u''
        if currency.position == 'before':
            pre = u'{symbol}\N{NO-BREAK SPACE}'.format(symbol=currency.symbol or '')
        else:
            post = u'\N{NO-BREAK SPACE}{symbol}'.format(symbol=currency.symbol or '')

        return u'{pre}{0}{post}'.format(formatted_amount, pre=pre, post=post)

    def _phone_get_country(self, partner):
        if 'country_id' in partner:
            return partner.country_id
        return self.env.user.company_id.country_id

    def _msg_sanitization(self, partner, field_name):
        number = partner[field_name]
        if number and _sms_phonenumbers_lib_imported:
            country = self._phone_get_country(partner)
            country_code = country.code if country else None
            try:
                phone_nbr = phonenumbers.parse(number, region=country_code, keep_raw_input=True)
            except phonenumbers.phonenumberutil.NumberParseException:
                return number
            if not phonenumbers.is_possible_number(phone_nbr) or not phonenumbers.is_valid_number(phone_nbr):
                return number
            phone_fmt = phonenumbers.PhoneNumberFormat.E164
            return phonenumbers.format_number(phone_nbr, phone_fmt)
        else:
            return number

    def _get_records(self, model):
        if self.env.context.get('active_domain'):
            records = model.search(self.env.context.get('active_domain'))
        elif self.env.context.get('active_ids'):
            records = model.browse(self.env.context.get('active_ids', []))
        else:
            records = model.browse(self.env.context.get('active_id', []))
        return records


    def _get_report_template(self,active_model):
        if active_model == 'sale.order':
            try:
                report = self.env.ref('roya_reports.test_quotation')
            except:
                template = self.env.ref('sale.email_template_edi_sale')
                report = template.report_template
        elif active_model == 'account.move':
            try:
                report = self.env.ref('roya_reports.test_thimar')
            except:
                template = self.env.ref('account.email_template_edi_invoice')
                report = template.report_template
        elif active_model == 'purchase.order':
            try:
                report = self.env.ref('roya_reports.purchase_quotation')
            except:
                if self.env.context.get('send_rfq', False):
                    template = self.env.ref('purchase.email_template_edi_purchase')
                else:
                    template = self.env.ref('purchase.email_template_edi_purchase_done')
                report = template.report_template

        elif active_model == 'stock.picking':
            try:
                report = self.env.ref('roya_reports.delivery_voucher')
            except:
                template = self.env.ref('stock.mail_template_data_delivery_confirmation')
                report = template.report_template
        elif active_model == 'account.payment':
            try:
                report = self.env.ref('roya_reports.paym_voucher_report_action')
            except:
                template = self.env.ref('account.mail_template_data_payment_receipt')
                report = template.report_template
        return report

    @api.model
    def default_get(self, fields):
        result = super(SendWAMessage, self).default_get(fields)
        active_model = self.env.context.get('active_model')
        Attachment = self.env['ir.attachment']
        receipt_data = self.env.context.get('receipt_data')
        if not active_model:
            return result
        res_id = self.env.context.get('active_id')
        if active_model == 'pos.order':
            rec = self.env[active_model].search([('pos_reference', '=', res_id)], order='id desc', limit=1)
            if not rec:
                return result
            res_id = rec.id
            res_name = rec.pos_reference and rec.pos_reference.replace('/', '_').replace(' ', '_')
            if receipt_data:
                try:
                    specific_paperformat_args = {
                        'data-report-margin-top': 10,
                        'data-report-margin-left': 40,
                    }
                    data1 = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>'
                    receipt_data = data1 + receipt_data
                    pdf = self.env['ir.actions.report']._run_wkhtmltopdf(
                        [receipt_data.encode()],
                        landscape=True,
                        specific_paperformat_args=specific_paperformat_args
                    )
                    res = base64.b64encode(pdf)
                    filename = res_name + '.pdf'
                    attachment_data = {
                        'name': filename,
                        'datas': res,
                        'type': 'binary',
                        'store_fname': filename,
                        'res_model': active_model,
                        'res_id': res_id,
                        'mimetype': 'application/pdf'
                    }
                    attachment_id = Attachment.create(attachment_data).id
                    result['attachment_ids'] = [(6, 0, [attachment_id])]
                except Exception:
                    traceback.print_exc()

                return result
        else:
            rec = self.env[active_model].browse(res_id)
            res_name = 'Invoice_' + rec.name.replace('/', '_') if active_model == 'account.move' else rec.name.replace('/', '_')
        msg = result.get('message', '')
        result['message'] = msg

        if not self.env.context.get('default_recipients') and active_model and hasattr(self.env[active_model], '_get_default_whatsapp_recipients'):
            model = self.env[active_model]
            if active_model == 'pos.order':
                records = self.with_context(active_id=res_id, active_ids=[res_id])._get_records(model)
            else:
                records = self._get_records(model)
            if active_model == 'res.partner' and records and self.env.context.get('from_multi_action'):
                records = records.filtered(lambda p: p.mobile and p.country_id)
            partners = records._get_default_whatsapp_recipients()
            phone_numbers = []
            no_phone_partners = []
            if active_model != 'res.partner':
                is_attachment_exists = Attachment.search([('res_id', '=', res_id), ('name', 'like', res_name + '%'), ('res_model', '=', active_model)], limit=1)
                if is_attachment_exists.ids:
                    is_attachment_exists.sudo().write({'res_id':False,'res_model':False})
                    # is_attachment_exists.sudo().unlink()
                attachments = []
                if active_model == 'pos.order':
                    if rec.mapped('account_move'):
                        report = self.env.ref('point_of_sale.pos_invoice_report')
                        report_service = res_name + '.pdf'
                    else:
                        return result
                else:
                    report = self._get_report_template(active_model)


                if report.report_type not in ['qweb-html', 'qweb-pdf']:
                    raise UserError(_('Unsupported report type %s found.') % report.report_type)
                res, format = report._render_qweb_pdf([res_id])
                res = base64.b64encode(res)
                if not res_name:
                    res_name = 'report.' + report_service
                ext = "." + format
                if not res_name.endswith(ext):
                    res_name += ext
                attachments.append((res_name, res))
                attachment_ids = []
                for attachment in attachments:
                    attachment_data = {
                        'name': attachment[0],
                        'datas': attachment[1],
                        'type': 'binary',
                        'res_model': active_model,
                        'res_id': res_id,
                    }
                    attachment_ids.append(Attachment.create(attachment_data).id)
                if attachment_ids:
                    result['attachment_ids'] = [(6, 0, attachment_ids)]

            for partner in partners:
                number = self._msg_sanitization(partner, self.env.context.get('field_name') or 'mobile')
                if number:
                    phone_numbers.append(number)
                else:
                    no_phone_partners.append(partner.name)
            if len(partners) > 1:
                if no_phone_partners:
                    raise UserError(_('Missing mobile number for %s.') % ', '.join(no_phone_partners))
            if partners:
                result['partner_ids'] = [(6, 0, partners.ids)]
        return result



    def action_send_msg(self):
        for partner in self.partner_ids:
            self.send_ws_msg(partner=partner)
        return True



class ScanWAQRCode(models.TransientModel):
    _name = 'whatsapp.scan.qr'
    _description = 'Scan WhatsApp QR Code'

    name = fields.Char()

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ScanWAQRCode, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu
        )
        if view_type == 'form':
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//img[hasclass('qr_img')]"):
                node.set('src', self.env.context.get('qr_image'))
            res['arch'] = etree.tostring(doc, encoding='unicode')
        return res

    def action_send_msg(self):
        res_id = self.env.context.get('wiz_id')
        if res_id:
            time.sleep(5)
            self.env['whatsapp.msg'].browse(res_id).action_send_msg()
        return True


class RetryWAMsg(models.TransientModel):
    _name = 'whatsapp.retry.msg'
    _description = 'Retry WhatsApp Message'

    name = fields.Char()

    def action_retry_send_msg(self):
        res_id = self.env.context.get('wiz_id')
        if res_id:
            time.sleep(5)
            self.env['whatsapp.msg'].browse(res_id).action_send_msg()
        return True
