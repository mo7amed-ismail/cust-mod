# -*- coding: utf-8 -*-

import os
import shutil

from odoo import api, fields, models, _


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    bevatel_auth = fields.Char('Whatsapp Bevatel Authorization')

    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        IrDefault = self.env['ir.default'].sudo()
        res.update({
            'bevatel_auth': IrDefault.get('res.config.settings', 'bevatel_auth'),
        })
        return res

    def set_values(self):
        super(ResConfigSettings, self).set_values()
        IrDefault = self.env['ir.default'].sudo()
        IrDefault.set('res.config.settings', 'bevatel_auth', self.bevatel_auth)

        return True


