# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    "name" : "Financial Reports For Sales Person ",
    "version" : "15.0.0.3",
    "category" : "Accounting",
    'summary': 'Multiple Sales Person Management Multi Sales Person app Multiple Unit Operating unit Sales Person Invoicing Sales Person financial Sales Person wise filter reports Sales Person reports Sales Person Accounting statement Financial Sales Person Reports for single company with Multi Sales Persones multi company',
    "description": """
      odoo multiple Sales Person accounting reports multiple Sales Person accounting reports multiple Sales Person accounting enterprise reports
 
    """,

     "depends" : ['account', 'account_accountant', 'account_reports', ],
    "data": [
            'views/search_template_view.xml',
            ],
    'assets': {
        'web.assets_backend': [
            'salesperson_accounting_report/static/src/js/custom_account_reports.js',
        ],
    },
    'qweb': [],
    "auto_install": False,
    "installable": True,

}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
