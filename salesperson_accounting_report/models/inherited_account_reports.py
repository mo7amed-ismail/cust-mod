# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _

class AccountReport(models.AbstractModel):
    _inherit = 'account.report'

    filter_user = None

    @api.model
    def _init_filter_user(self, options, previous_options=None):
        if not self.filter_user:
            return options
        options['user'] = True
        options['user_ids'] = previous_options and previous_options.get('user_ids') or []
        selected_user_ids = [int(partner) for partner in options['user_ids']]
        selected_useres = selected_user_ids and self.env['res.users'].browse(selected_user_ids) or self.env['res.users']
        options['selected_user_ids'] = selected_useres.mapped('name')
        return options

    @api.model
    def _get_options(self, previous_options=None):
        # OVERRIDE
        options = super(AccountReport, self)._get_options(previous_options)
        self._init_filter_user(options, previous_options)
        return options


    def _set_context(self, options):
        ctx = super(AccountReport, self)._set_context(options)
        if options.get('user_ids'):
            user_ids = self.env['res.users'].browse([int(user) for user in options.get('user_ids',[])])
            ctx['user_ids'] = user_ids.ids
        return ctx


    def get_report_informations(self, options):
        '''
        return a dictionary of informations that will be needed by the js widget, manager_id, footnotes, html of report and searchview, ...
        '''
        info = super(AccountReport, self).get_report_informations(options)
        if options and options.get('user'):
            options['selected_user_ids'] = [self.env['res.users'].browse(int(user)).name for user in options.get('user_ids',[])]
        return info
