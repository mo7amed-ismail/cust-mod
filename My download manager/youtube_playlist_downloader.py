from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk
from pytube import Playlist
from pytube import YouTube
import arabic_reshaper
import bidi.algorithm
import threading
import sys
import os
import webbrowser

root = Tk()
root.title("Playlist Downloader")
# root.iconbitmap("icon.ico")
root.geometry("600x350")
root.resizable(FALSE, FALSE)


# functions
def browse():
    directory = filedialog.askdirectory(title="Save Video")
    folder_link.delete(0, "end")
    folder_link.insert(0, directory)


# error screaen
def error(field_name):
    error_screen = Toplevel(root)
    error_screen.geometry("150x90")
    error_screen.title("Warning!")
    Label(error_screen, text=f"{field_name} is required")


def download():
    status.config(text="Status : Downloading.....")
    link = ytlink.get()
    if link == '':
        messagebox.showerror('Error', 'Put The Link Before Download :(')
    folder = folder_link.get()
    if folder == '':
        messagebox.showerror('Error', 'Choose the Directory First :(')
    all_videos = list(Playlist(link))
    video_enum = 1
    print("-------------------------------------------------------------------------------------------------")
    for i in all_videos:
        print(str(video_enum) + " : " + str(i))
        video_enum = video_enum + 1
    print("-------------------------------------------------------------------------------------------------")
    print("\n")
    available_streams = YouTube(link, use_oauth=True, allow_oauth_cache=True).streams.filter(progressive=True, file_extension="mp4")
    for stm in available_streams:
        print(stm)
    stream = select_stream.get()
    if stream:
        print(stream)
        i = 1
        for video in all_videos:
            vid = YouTube(video, use_oauth=True, allow_oauth_cache=True).streams.filter(progressive=True, file_extension="mp4", res=stream).get_by_resolution(stream)
            vido = vid.title
            print(str(i)+ '- Video Name: '+ str(vido))
            i = i+1
            text = str(vido)
            reshape_text = arabic_reshaper.reshape(text)
            bidi_text = bidi.algorithm.get_display(reshape_text)
            video_label.config(text=bidi_text)
            vid.download(folder)
        status.config(text="Status : download completed successfully :).....")
        print("download completed successfully :)")


def restart():
    if ytlink.get() == "" and folder_link.get() == "":
        messagebox.showerror("Error", "Empty Fields")
    else:
        ytlink.delete(0, "end")
        folder_link.delete(0, "end")
        video_label.config(text="Video Name: ")
        status.config(text="Status : Ready....")
    os.execv(sys.executable, ["python"] + sys.argv)


# youtube logo
ytlogo = PhotoImage(file="/home/ma7moud/odoo folders/Courses/My download manager/youtube logo.png")
yt_title = Label(root, image=ytlogo)
yt_title.place(relx=0.50, rely=0.25, anchor="center")
# youtube link
ytlabel = Label(root, text="Youtube Link")
ytlabel.place(x=25, y=150)
ytlink = Entry(root, width=50)
ytlink.place(x=140, y=150)
# download folder
folder_label = Label(root, text="Download Folder")
folder_label.place(x=25, y=183)
folder_link = Entry(root, width=40)
folder_link.place(x=140, y=183)
# video name
video_label = Label(root, text="Video Name: ", width=600, fg="black", bg="white")
video_label.place(relx=0.50, rely=0.1, anchor="center")


# stream selection
stream_label = Label(root, text="Quality")
stream_label.place(x=25, y=220)
streams = ['360p', '720p']
select_stream = ttk.Combobox(root, values=streams, width=10)
select_stream.place(x=100, y=220)
select_stream.set('360p')

# browse button
browse = Button(root, text="Browse", command=browse)
browse.place(x=470, y=180)
# download button
download = Button(root, text="Download", command=threading.Thread(target=download).start)
download.place(x=280, y=220)
# clear data and restart button
restart = Button(root, text="Clear", command=threading.Thread(target=restart).start)
restart.place(x=400, y=220)

# status bar
status = Label(root, text="Status : Ready....", font="Calibre 10 italic", fg="black", bg="white", anchor="w")
status.place(rely=1, anchor="sw", relwidth=1)

root.mainloop()
