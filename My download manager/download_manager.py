from pytube import YouTube
from tkinter import filedialog
url = input("Please enter the url: ")
all_streams = YouTube(url, use_oauth=True, allow_oauth_cache=True).streams.filter( file_extension="mp4")
v = -1
for i in all_streams:
    v = v+1
    print(str(v)+" : "+str(i))
stm_input = int(input("Please choose video stream: "))
print("Please choose directory to save: ")
folder_name = filedialog.askdirectory()
download = all_streams[int(stm_input)].download()
print("download completed successfully :)")
