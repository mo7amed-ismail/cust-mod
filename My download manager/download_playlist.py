from tkinter import *
from tkinter import filedialog
from pytube import Playlist
from pytube import YouTube
import webbrowser

url_input = input("Put Your Url Here: ")
print('\n')
process_list = ['Open','Download']
list_enum = 1
for i in process_list:
    print(f"{list_enum} : {i}")
    list_enum=list_enum+1
process_input = int(input("Please Choose Process To complete: "))
if process_input == 1:
    webbrowser.open(url_input)
elif process_input == 2:
    print("\n")
    all_videos = Playlist(url_input).videos
    video_enum = 1
    print("-------------------------------------------------------------------------------------------------")
    for i in all_videos:
        print(str(video_enum)+" : "+str(i))
        video_enum =video_enum+1
    print("-------------------------------------------------------------------------------------------------")
    print("\n")
    print("""
****************************************************
        Answer Yes Or No
****************************************************
    """)
    conf_input = input("Do You Want To Continue(y/n): ")
    if conf_input.lower().startswith('y'):
        videos = list(Playlist(url_input))
        video = YouTube(videos[0], use_oauth=True, allow_oauth_cache=True)
        available_streams = video.streams.filter(progressive=True, file_extension="mp4")
        stm_enum = 0
        for stm in available_streams:
            print(f"{stm_enum} : {stm}")
            stm_enum = stm_enum + 1
        stm_input = int(input("Please choose video stream: "))
        print("Please choose directory to save: ")
        folder_name = filedialog.askdirectory()
        for i in range(len(videos)):
            print(i, '-', videos[i])
            video = YouTube(videos[i], use_oauth=True, allow_oauth_cache=True)
            download = video.streams[int(stm_input)].download(folder_name)
        print("download completed successfully :)")



    elif conf_input.lower().startswith('n'):
        print("Ok Bye")
        exit()
    else:
        print("Choose right answer :( ")
        exit()