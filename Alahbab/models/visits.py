from odoo import models, fields, api, _
from odoo.exceptions import ValidationError



class visits(models.Model):
    _name = 'visits_following'
    _rec_name = 'person_id'


    state=fields.Selection([('visited','Was Visited neerly'),('need','Need To be visited')])
    person_id=fields.Many2one('res.partner',domain="[('da3wa','=',True)]", string='Person')
    mosque=fields.Char(related='res.partner.main_mosque',string='Main Mosque')
    last_visit_date=fields.Date('Last visit Date')
    days_aft_ls_v=fields.Integer(compute='_get_days',string='Dayes after last visit')


    @api.depends('last_visit_date')
    def _get_days(self):
        self.days_aft_ls_v=(fields.Date.today()-self.last_visit_date).days


    @api.onchange('days_aft_ls_v')
    def visited(self):
        for i in self:
            if i.days_aft_ls_v:
                if i.days_aft_ls_v > 15:
                    i.state='visited'


    # @api.model
    # def scheduel_daily_follow_up(self):
    #     self.env['visits'].search([('days_aft_ls_v','>=',15)]).write({'state':'visited'})



class addNewvisit(models.TransientModel):
    _name = 'new.visit.wiz'

    visited_persons_ids=fields.Many2many('visits_following', string='Visited Persons',
                                 default=lambda self: self._context.get('active_ids'))
    visit_date=fields.Date('Visit Date')

    def add_new_visit(self):
        for v in self.visited_persons_ids:
            v.write({'last_visit_date':self.visit_date,'state':'visited'})

