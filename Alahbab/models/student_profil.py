from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class student(models.Model):
    _description = 'This table has all information of studets'
    # _name = 'res.partner'
    _inherit = ['res.partner']
    # ,'mail.thread.cc',
    #         'mail.thread.blacklist',
    #        # 'mail.thread.phone',
    #         'mail.activity.mixin',
    #         #'utm.mixin',
    #        # 'format.address.mixin',
    #        ]
    '''Education information'''

    da3wa = fields.Boolean()
    state = fields.Selection([('still_student', 'Student'),
                              ('graduated', 'Graduted'),
                              ('not_educated', 'Not Educated')], string='Education State')
    education_type = fields.Selection([('high_education', 'High Education'),
                                       ('intermediate_education', 'Intermediate Education'),
                                       ('high_school', 'High School')]
                                      , states={'not_educated': [('invisible', True)]}, string='Education Type')
    grade = fields.Char(string='Grade')
    academic_level = fields.Selection([('0', 'Low'),
                                       ('1', 'Medium'),
                                       ('2', 'High'),
                                       ('3', 'Very High'), ], states={'not_educated': [('invisible', True)]},
                                      string='Acadimic level')
    fuculty = fields.Char('Fuculty')

    '''معلومات خاصة بالعبادة والدعوى الى الله'''
    '''Information about worship and supplication to Allah'''

    main_mosque = fields.Char('The Main mosque')
    '''نسبة الالتزام'''
    commitment_rate = fields.Float('Commitment rate', compute='_get_com_rate')
    efficient = fields.Selection([('afficirnt', 'Afficient'), ('not_afficient', 'Not Afficient'), ], 'Efficient')
    friends_ids = fields.Many2many('res.partner', 'student_frinds_rel', 'student_id', 'frind_id',
                                   string='Student Frinds')
    '''الاعمال المقامية المحافظ عليها'''
    conserved_deeds_ids = fields.Many2many('maqam.deed', 'student_deed_rel', 'student_id', 'deed_id',
                                           string='Conserved Deeds')

    '''معلومات خاصة بالخروج'''
    '''Migration details'''
    num_of_migrations = fields.Integer('Number of migratins')
    max_migration = fields.Integer('Max Migration')
    first_mig_date = fields.Date('First migration date')
    last_mig_date = fields.Date('Last migration date')
    notes = fields.Text('Notes')

    def write(self, vals):
        old_deeds_ids = self.conserved_deeds_ids
        res = super(student, self).write(vals)
        new_deeds_ids = self.conserved_deeds_ids
        print(old_deeds_ids)
        print(new_deeds_ids)
        if len(new_deeds_ids) > len(old_deeds_ids):
            new_ids = new_deeds_ids - old_deeds_ids
            print(new_ids)
            for i in new_ids:
                if self.browse(self.id) not in i.students_ids:
                    i.write({'students_ids': [(4, self.id)]})
        elif len(new_deeds_ids) < len(old_deeds_ids):
            new_ids = old_deeds_ids - new_deeds_ids
            for i in new_ids:
                if self.browse(self.id) in i.students_ids:
                    i.write({'students_ids': [(3, self.id)]})
        return res

    @api.depends('conserved_deeds_ids')
    def _get_com_rate(self):
        for stu in self:
            if stu.conserved_deeds_ids:
                cons_deeds = len(stu.conserved_deeds_ids)
                total_deeds = self.env['maqam.deed'].search_count([])
                stu.commitment_rate = (cons_deeds / total_deeds) * 100
            else:
                stu.commitment_rate = None

    def newmigration(self):
        return {'name': _('Add new migration'),
                'view_mode': 'form',
                'res_model': 'add.migration',
                'domain': [],
                'target': 'new',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {'default_mig_date': fields.Date.today()}}


