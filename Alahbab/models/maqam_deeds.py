from odoo import models, fields, api
from odoo.exceptions import ValidationError


class maqam_deeds(models.Model):
    _name = 'maqam.deed'



    name=fields.Char('Deed Name')
    mosque=fields.Char('Moaque')
    daily_time=fields.Datetime()
    conservation=fields.Boolean('Conservation', default=True)
    notes=fields.Text()
    students_ids=fields.Many2many('res.partner',string='Students')
    students_count=fields.Integer('Number of students')




    def write(self, vals):
        old_students_ids = self.students_ids
        res = super(maqam_deeds, self).write(vals)
        new_students_ids = self.students_ids
        if len(new_students_ids) > len(old_students_ids):
            added_ids = new_students_ids - old_students_ids
            for i in added_ids:
                if self.browse(self.id) not in i.conserved_deeds_ids:
                    i.write({'conserved_deeds_ids': [(4, self.id)]})
        elif len(new_students_ids) < len(old_students_ids):
            deleted_ids = old_students_ids - new_students_ids
            for i in deleted_ids:
                if self.browse(self.id) in i.conserved_deeds_ids:
                    i.write({'conserved_deeds_ids': [(3, self.id)]})
        return res

