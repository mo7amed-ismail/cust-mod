from odoo import models, fields, api, _
from odoo.exceptions import ValidationError





class AddNewMigration(models.TransientModel):

    _name = 'add.migration'

    students_ids=fields.Many2many('res.partner',default=lambda self: self._context.get('active_ids'))
    migration_period = fields.Integer('Migration period')
    responsible = fields.Char('Responsible')
    fro_mosque=fields.Selection(lambda self: self._get_mosques,string='From')
    to_mosque=fields.Char('To')
    mig_date=fields.Date('Migration Date')


    def addmigr(self):
        if self.migration_period:
            for student in self.students_ids:
                student.num_of_migrations+=1
                student.last_mig_date=self.mig_date
                if self.migration_period > student.max_migration:
                    student.write({'max_migration': self.migration_period})
                student.message_post(body=f"<b>A New migration</b> {self.migration_period} Days <b>with</b> {self.responsible} <b>From</b> {self.fro_mosque} <b>To</b> {self.to_mosque} <b>At</b> {self.mig_date}")
        else:
            raise ValidationError(_('Migration Period didn\'t specifyed'))
        return {}


    @property
    def _get_mosques(self):
        mosques_list = [(i.main_mosque) for i in self.env['res.partner'].search([('da3wa', '=', True)]) if i.main_mosque]
        for mos in mosques_list:
            if mosques_list.count(mos) > 1:
                mosques_list.remove(mos)
        return [(m.lower(), m) for m in mosques_list]





